CHANGES


2019-08-02 (nb)

Improved shield_edu4xio_buzzer_tone function.

Improved shield_edu4xio_photores function.

FIX: Bug for ADLAR left shift result. Small fix in shield_edu4xio_buzzer_tone.


2019-01-04 (nb)

Fix in shield_edu4xio_init - specified Voltage Reference.


2018-09-18 (nb)

IMPORTANT CHANGE: The "blocktinu_api" module was moved to its own repository at https://bitbucket.org/tinusaur/blocktinu-backend/.

Removed blocktinu_api from this repository.


2018-09-09 (nb)

Updated text files.

IMPORTANT CHANGE: The "blocktinu_backend" module was moved to its own repository at https://bitbucket.org/tinusaur/blocktinu-build/.

IMPORTANT CHANGE: The "blocktinu_webui" module moved to its own repository at https://bitbucket.org/tinusaur/blocktinu-webui/.

Updated TODO list.


2018-07-26 (nb)

Fixed the digital pins number in board_profile.

Fixed the digital read init code.

Fixed the digital read code.

New features. Login, Register, Save and Load code and block (gm)

Added confirmation dialog for loading from server.


2018-07-20 (nb)

Updated installation text files.


2018-07-17 (nb)

Updated text files.


2018-04-22 (nb)

Fixed success/failure handling while compiling the source code.


2018-04-22 (nb)

Updated text and formatting.


2018-04-22 (nb)

Load (at startup) and save (on each build) the source code to/from browser local storage.


2018-04-22 (nb)

Loading C template from a file.


2018-04-22 (nb)

Fixed and improved Makefile files.


2018-04-22 (nb)

Added build procedures logs to the response from backend.


2018-04-20 (nb)

Back-end updated to the latest blocktinu_clibt85 module.


2018-04-20 (nb)

Removed the use of "tinublockly" in the blocktinu_clibt85 module - in file names, references and texts.


2018-04-20 (nb)

The back-end C templates are put in a new folder. Updated text in files. Few other small fixes.


2018-04-17 (nb)

Updated INSTALL and UPDATE text documents.


2018-04-13 (nb)

Bugfix: Compilation error when using bool/true/false. Added "stdbool.h" to the C template.


2018-04-13 (nb)

Updated installation procedures.


2018-04-12 (nb)

Bugfix: The WEBUI is always uploading the block-generated source code.


2018-04-12 (gm)

Added ACE editor for native c/cpp code


2018-03-18 (nb)

Fixed bug: error while generating code, when var assignment.


2018-03-18 (nb)

Added UPDATE-webui.txt file.


2018-03-18 (nb)

Update blocktinu-backend.js-distrib sample config.


2018-03-18 (nb)

Improved "index.html", added branding/logo.


2018-03-18 (nb)

Top menu rearranged, handling function improved.


2018-03-18 (nb)

Add new "INSTALL-webui.txt" file.


2018-03-18 (nb)

Fixed bug: variable with wrong names. Updated to the latest implementation of original library.

- Fixed bug: variable with wrong names (intern. id instead of name) when assigning a value.
- Updated Blockly.Tinusaur.init to the latest implementation of original library (JavaScript).
- Source code reformatting.


2018-03-18 (nb)

The Blocktinu back-end configuration moved to a separate file.


2018-03-15 (nb)

Improved back-end installation procedures. Added "build.sh" build script.


2018-03-15 (nb)

Fixed bug: not generating source code with variables.


2018-03-15 (nb)

Updated, added TODO and other files.

Updated, added, rearranged - examples and tools on the menu.


2018-03-11 (nb)

Upgraded to the latest Blockly library. Fixed incompatibility in the tinusaur.js file. Updated LIBS-EXT-webui.

- Upgraded to the Blockly library from Mar 9, 2018 (7ba9e5e).
- Fixed incompatibility in the "blocktinu_webui/libs/tinusaur/generators/tinusaur.js" file.
- Updated the "LIBS-EXT-webui.txt" file.


2018-03-02 (nb)

Added source code preview pane. Other small improvements, fixes, additions.

- Improved WEBUI: added source preview pane, output ins now a .bthex file, added use of font-awesome library.
- Improved back-end: fixed new folder attributes.
- Updated INSTALL-backend.txt file.
- New LIBS-EXT-webui.txt file was added.


2017-08-25 (nb)

Log file name changed.

Added new INSTALL-backend.txt file.

Updated TODO.


2017-08-22 (nb)

Added header "Access-Control-Allow-Origin" for Cross-origin handling. Light refactoring, variables renamed.

- Added header "Access-Control-Allow-Origin" for Cross-origin handling.
- Light refactoring, variables renamed.


2017-08-19 (gm)

Keep blocks state in local storage on every activity


2017-08-19 (nb)

Updated TODO list.


2017-08-19 (nb)

Fixed back-end URL to reflect the updated folders structure. Updated comments and texts.

- Fixed back-end URL to reflect the updated folders structure.
- Updated comments and texts.


2017-06-10 (nb)

Added TODO file. Adjusted the formatting of the generated source code.

- Added TODO file.
- Adjusted the formatting of the generated source code.
- Some more texts switched back to English.


2017-06-10 (nb)

Added the "backend" file to the repository.


2017-06-10 (nb)

Added the "webui" files to the repository. Added .hgignore file.

- Added the "webui" files to the repository.
- Added the .hgignore file to the repository.

2017-06-10 (nb)

Improved the testing main.c script in the clibt85. Other fixes.

- Improved the testing main.c script in the clibt85.
- Fixed invalid character in the Makfile file.


2017-05-18 (nb)

Added README, LICENSE files.


2017-05-18 (nb)

Changed repository name. Folder "tinublockly_clib85" renamed to "blocktinu_clibt85".


2017-05-18 (nb)

Fixed bug in the shield_edu4xio_init. Updated comments.


2017-05-12 (nb)

Added new shield_edu4xio_button function.


2017-05-12 (nb)

Added new shield_edu4xio_buzzer_tone, shield_edu4xio_photores functions. Removed NULL definition, conflicts.

- Added new shield_edu4xio_buzzer_tone, shield_edu4xio_photores functions.
- Removed NULL definition, conflicts.


2017-05-12 (nb)

Added definitions for FALSE TRUE NULL.


2017-05-12 (nb)

Files added to the repository.


