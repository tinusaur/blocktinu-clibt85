/**
 * Blocktinu-CLibT85 - Testing scripts
 * @author Neven Boyanov
 * This is part of the Tinusaur/Blocktinu-CLibT85 project.
 * ----------------------------------------------------------------------------
 *  Copyright (c) 2021 Tinusaur (https://tinusaur.com). All rights reserved.
 *  Distributed as open source under the MIT License (see the LICENSE.txt file)
 *  Please, retain in your work a link to the Tinusaur project website.
 * ----------------------------------------------------------------------------
 * Source code available at: https://gitlab.com/tinusaur/blocktinu-clibt85
 */

// ============================================================================

#include <stdint.h>
#include <avr/io.h>
#include <util/delay.h>

#include "blocktinu_oled/blocktinu_oled.h"

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//                 ATtiny85
//               +----------+    (-)--GND--
//      (RST)--> + PB5  Vcc +----(+)--VCC--
// ---[OWOWOD]---+ PB3  PB2 +----[TWI/SCL]-
// --------------+ PB4  PB1 +--------------
// --------(-)---+ GND  PB0 +----[TWI/SDA]-
//               +----------+
//                 Tinusaur
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// NOTE: If you want to reassign the SCL and SDA pins and the I2C address
// do that in the library source code and recompile it so it will take affect.

// ----------------------------------------------------------------------------

uint16_t demo_rand(void) {
	static uint16_t r; // NOTE: For more "real" random numbers this should be 32bit.
	// r = (r * 17 + 83) % 0x3fff;	// Generate a pseudo random number.
	r = ((r << 4) + r + 83) & 0xffff;	// Optimized (above)
	return r >> 2;
}

// ----------------------------------------------------------------------------

#define TESTING_DELAY 2000

int main(void) {
	// ---- Initialization ----
	oled_init();

	// ---- Main Loop ----
	for (;;) {

		oled_clear();	// Clear the screen.
		for (uint8_t c = 3; c > 0; c--) {
			oled_fill();
			oled_clear();
		}
		// ---- Print some text on the screen ----
		// NOTE: Screen width - 128, that is 16 symbols per row.
		oled_string(0, 0, "<<< TINUSAUR >>>");
		oled_string(0, 1, "-Blocktinu-OLED-");
		// oled_string(0, 2, "Hello, World! :)");
		oled_string(0, 2, "!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[5]^_`abcdefghijklmnopqrstuvwxyz\x7b\x7c\x7d\x7e\x7f\x80");
		_delay_ms(TESTING_DELAY);
		oled_string(0, 6, "----------------");
		oled_string(0, 7, "[ tinusaur.com ]");
		_delay_ms(TESTING_DELAY);

		// ---- Print some  numbers on the screen ----
		oled_clear();
		oled_string(0, 0, "a:----- X=-----");
		oled_string(0, 1, "  -----   -----");
		oled_string(0, 2, "b:----- Y=-----");
		oled_string(0, 3, "  -----   -----");
		oled_string(0, 4, "c:----- Z=-----");
		oled_string(0, 5, "  -----   -----");
		oled_string(0, 6, "F(-----)=>-----");
		oled_string(0, 7, "---------------");
		for (uint8_t k = 100; k > 0; k--) {
			for (uint8_t j = 0; j <= 3; j++) {
				for (uint8_t i = 0; i <= 1; i++) {
					oled_nump((i << 3) + 2, j << 1, demo_rand());
				}
			}
		}
		_delay_ms(TESTING_DELAY);
	}

	return 0; // Return the mandatory for the "main" function int value - "0" for success.
}

// ============================================================================
