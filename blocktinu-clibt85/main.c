/**
 * Blocktinu - CLibT85
 *
 * @created 2017-05-12
 * @author Neven Boyanov
 *
 * This is part of the Tinusaur/Blocktinu project.
 *
 * ----------------------------------------------------------------------------
 *  Copyright (c) 2024 Tinusaur (https://tinusaur.com). All rights reserved.
 *  Distributed as open source under the MIT License (see the LICENSE.txt file)
 *  Please, retain in your work a link to the Tinusaur project website.
 * ----------------------------------------------------------------------------
 * Source code available at: https://gitlab.com/tinusaur
 */

// ============================================================================
// **** NOTE: THIS LIBRARY IS NO LONGER IN USE ****
// ============================================================================

#include <stdlib.h>
#include <stdint.h>
#include <avr/io.h>
#include <util/delay.h>

#include "blocktinu-clibt85.h"

// ----------------------------------------------------------------------------

int main(void) {
	
	for (;;) { // Infinite loop
	}

	return 0; // Return the mandatory result value.
}

// ============================================================================
